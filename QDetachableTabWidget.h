/**
  @file         QDetachableTabWidget.h
  @description  This module contains definitions for detachable tab widget
  @author       DronCode <alexandrleutin@gmail.com>
  @license      GPLv2
  @version      0.0.1
  @todolist     1) Create policy for dialog destroy (restoreTab or closeTab)
                2) Create policy for tab detach (emit tabClose or not)
  @bugs         1) Unable to swap tabs (bug with event handling, threshold constants must be used instead our logic)
**/
#pragma once

#include <QMouseEvent>
#include <QTabWidget>
#include <QTabBar>
#include <QObject>
#include <QDialog>
#include <QPoint>

/**
 * @class QDetachedTabDialog
 * @brief Basic wrapper for QDialog for content managment features (like save pointer to original content for restore process)
 */
class QDetachedTabDialog : public QDialog
{
    Q_OBJECT
public:
    QDetachedTabDialog(QWidget* contents, QWidget* parent = nullptr);

    /**
     * @fn      getContents
     * @brief   getter for private saved pointer to contents
     * @return  pointer to contents
     * @note    m_contents pointer could be invalid
     */
    QWidget* getContents();
private:
    QWidget* m_contents = nullptr; ///< Pointer to contents
};


/**
 * @class QDetachableTabBar
 * @brief The implementation of tab bar. Here located main logic of 'detach' process.
 */
class QDetachableTabBar : public QTabBar
{
    Q_OBJECT

    const int InvalidTabIndex = -1; ///< Default value for index (valid index must be greater or equal than zero!)
public:
    explicit QDetachableTabBar(QWidget* parent = nullptr);

protected:
    void mousePressEvent(QMouseEvent *) override;
    void mouseReleaseEvent(QMouseEvent *) override;
Q_SIGNALS:
    /**
     * @fn     tabDetachRequest
     * @brief  this signal will be emitted when tab bar detects that mouse pointer released outside of widget's rect area
     * @param  targetTabIndex index of tab, who will be detached from QTabWidget
     * @param  mousePoint point coordinates of mouse pointer (could be helpful for animation in future)
     */
    void tabDetachRequest(int targetTabIndex, const QPoint& mousePoint);
private:
    int m_cathedTabIndex = InvalidTabIndex; ///< Index of catched tab

private:
    void reset();
};

/**
 * @class QDetachableTabWidget
 * @brief Tab widget core implementation here.
 */
class QDetachableTabWidget : public QTabWidget
{
    Q_OBJECT
public:
    explicit QDetachableTabWidget(QWidget* parent = nullptr);

    enum class ChildDialogClosePolicyPolicy {
        CloseDialogAfterParentDestroyed,
        SaveDialogAfterParentDestroyed
    };
    void setChildDialogClosePolicy(ChildDialogClosePolicyPolicy policy);
    ChildDialogClosePolicyPolicy getChildDialogClosePolicy() const;
private Q_SLOTS:
    void onDetachRequest(int tabIndex, const QPoint& mousePoint);
    void onDialogInstanceFinished();

private:
    ChildDialogClosePolicyPolicy m_childDialogClosePolicy = ChildDialogClosePolicyPolicy::CloseDialogAfterParentDestroyed;
};
