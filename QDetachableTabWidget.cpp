/**
  @file         QDetachableTabWidget.cpp
  @description  Core implementation and some logic here
  @author       DronCode <alexandrleutin@gmail.com>
  @license      GPLv2
**/

#include "QDetachableTabWidget.h"

#include <QVBoxLayout>

QDetachedTabDialog::QDetachedTabDialog(QWidget* contents, QWidget* parent)
    : QDialog(parent)
    , m_contents(contents)
{
}

QWidget* QDetachedTabDialog::getContents()
{
    return m_contents;
}

QDetachableTabBar::QDetachableTabBar(QWidget* parent)
    : QTabBar(parent)
{
}

void QDetachableTabBar::mousePressEvent(QMouseEvent* event)
{
    QTabBar::mousePressEvent(event);    /// process parent first

    if (rect().contains(event->pos()))
    {
        m_cathedTabIndex = tabAt(event->pos());
    }
}

void QDetachableTabBar::mouseReleaseEvent(QMouseEvent* event)
{
    QTabBar::mouseReleaseEvent(event);

    if (!rect().contains(event->pos()))
    {
        emit tabDetachRequest(m_cathedTabIndex, event->pos());
        reset();
    }
}

void QDetachableTabBar::reset()
{
    m_cathedTabIndex = InvalidTabIndex;
}

QDetachableTabWidget::QDetachableTabWidget(QWidget* parent)
    : QTabWidget(parent)
{
    setMovable(true);
    setTabsClosable(true);

    QDetachableTabBar* customBar = new QDetachableTabBar(this);

    connect(customBar, SIGNAL(tabDetachRequest(int, const QPoint&)), SLOT(onDetachRequest(int, const QPoint&)));

    setTabBar(customBar);
}

void QDetachableTabWidget::setChildDialogClosePolicy(QDetachableTabWidget::ChildDialogClosePolicyPolicy policy)
{
    m_childDialogClosePolicy = policy;
}

QDetachableTabWidget::ChildDialogClosePolicyPolicy QDetachableTabWidget::getChildDialogClosePolicy() const
{
    return m_childDialogClosePolicy;
}

void QDetachableTabWidget::onDetachRequest(int tabIndex, const QPoint& mousePoint)
{
    QWidget* content = widget(tabIndex);
    QRect contentGeom = content->frameGeometry();
    QString contentTabTitle = tabText(tabIndex);

    QVBoxLayout* newLayout = new QVBoxLayout();
    newLayout->addWidget(content);
    content->show();

    QWidget* ownerByClosePolicy = m_childDialogClosePolicy == ChildDialogClosePolicyPolicy::CloseDialogAfterParentDestroyed ? this : nullptr;

    QDetachedTabDialog* dialog = new QDetachedTabDialog(content, ownerByClosePolicy);
    dialog->setGeometry(contentGeom);
    dialog->setWindowTitle(contentTabTitle);
    dialog->setWindowModality(Qt::WindowModality::NonModal);
    dialog->setLayout(newLayout);
    dialog->move(mousePoint);

    connect(dialog, SIGNAL(finished(int)), SLOT(onDialogInstanceFinished()));

    dialog->show();
}

void QDetachableTabWidget::onDialogInstanceFinished()
{
    QDetachedTabDialog* emitter = qobject_cast<QDetachedTabDialog*>(sender());

    QWidget* contents = emitter->getContents();
    contents->setParent(this);

    int indexOfNewTab = addTab(contents, emitter->windowTitle());
    setCurrentIndex(indexOfNewTab);
}
